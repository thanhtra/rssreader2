package com.example.tulinh.rssreader2;

import android.util.Log;

import java.io.Serializable;

/**
 * Created by TuLinh on 10/6/2015.
 */
public class Item implements Serializable{
    private String title;
    private String link;
    private String description;
    private String pubDate;

    public Item() {
        Log.d("Item", "new item created");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String toString() {
        return this.pubDate + "\n" + this.title;
    }
}
