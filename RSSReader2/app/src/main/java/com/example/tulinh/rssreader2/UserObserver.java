package com.example.tulinh.rssreader2;

/**
 * Created by TuLinh on 10/6/2015.
 */
public interface UserObserver {
    void update();
}
