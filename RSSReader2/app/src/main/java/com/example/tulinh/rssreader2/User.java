package com.example.tulinh.rssreader2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by TuLinh on 10/6/2015.
 */
public class User {

    private List<Feed> feeds;
    private List<UserObserver> observers;
    private static User ourInstance = new User();

    public static User getInstance() {
        return ourInstance;
    }

    private User() {
        this.feeds = new ArrayList<>();
        this.observers = new ArrayList<>();
        this.initializeFeeds();
    }

    private void initializeFeeds() {
        this.feeds.add(new Feed("VOA", "http://www.voatiengviet.com/api/z$uyietpv_"));
        this.feeds.add(new Feed("Iltalehti - Jalkapallo","http://www.iltalehti.fi/rss/jalkapallo.xml"));
        this.feeds.add(new Feed("BBC - Top Stories", "http://feeds.bbci.co.uk/news/rss.xml"));
    }


    public void addFeed(Feed feed) {
        if (!this.feeds.contains(feed)){
         this.feeds.add(feed);
        }
    }

    public List<Feed> getFeeds() {
        return this.feeds;
    }

    public void addObserver(UserObserver o) {
        this.observers.add(o);
    }

    public void feedUpdated() {
        for (UserObserver o : this.observers) {
            o.update();
        }
    }

}
