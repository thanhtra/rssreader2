package com.example.tulinh.rssreader2;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by TuLinh on 10/8/2015.
 */
public class XMLHandler implements Runnable{
    private Feed currentFeed;
    private List<Item> items;
    private Item currentItem;
    private String text;
    private String urlString;

    public XMLHandler(Feed feed) {
        this.currentFeed = feed;
        this.items = currentFeed.getItems();
        this.urlString = this.currentFeed.getUrlString();
    }

    public List<Item> getItems() {
        return this.items;
    }

    public void parse(InputStream is) {

        XmlPullParserFactory factory;
        XmlPullParser parser;
        try{
            factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            // Create a parser
            parser = factory.newPullParser();

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(is, null);
            // variable primed: determines if parsing inside an Item
            boolean primed = false;

            int eventType = parser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = parser.getName();

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (tagName.equalsIgnoreCase("item")) {
                            primed = true;
                            // create a new instance of currentItem
                            currentItem = new Item();
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if (tagName.equalsIgnoreCase("item")) {
                            // add currentItem object to items
                            items.add(currentItem);
                            primed = false;
                        } else if (tagName.equalsIgnoreCase("title")) {
                            if (primed) {
                                currentItem.setTitle(text);
                            }
                        } else if (tagName.equalsIgnoreCase("link")) {
                            if (primed) {
                                currentItem.setLink(text);
                            }
                        } else if (tagName.equalsIgnoreCase("description")) {
                            if (primed) {
                                currentItem.setDescription(text);
                            }
                        } else if (tagName.equalsIgnoreCase("pubDate")) {
                            if (primed) {
                                currentItem.setPubDate(text);
                            }
                        }
                        break;

                    default:
                        break;
                }
                eventType = parser.next();
            }

            //call a method to update ui
            User.getInstance().feedUpdated();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            URL url = new URL(this.urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(15000);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);

            // start the query
            connection.connect();
            InputStream stream = connection.getInputStream();

            //parsing XML through the input stream
            this.parse(stream);


            stream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
