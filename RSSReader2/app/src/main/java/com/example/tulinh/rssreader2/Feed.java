package com.example.tulinh.rssreader2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TuLinh on 10/6/2015.
 */
public class Feed implements Comparable<Feed>, Serializable{
    private List<Item> items;
    private String title;
    private String urlString;

    public Feed(String title, String url) {
        this.items = new ArrayList<>();
        this.title = title;
        this.urlString = url;
    }

    public String getUrlString() {
        return this.urlString;
    }

    public List<Item> getItems() {
        return this.items;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Feed(String url) {
        this("",url);
    }

    public void addItem(Item item) {
        this.items.add(item);
    }

    public String toString() {
        return this.title;
    }

    @Override
    public int compareTo(Feed another) {
        return this.urlString.compareTo(another.getUrlString());
    }
}
