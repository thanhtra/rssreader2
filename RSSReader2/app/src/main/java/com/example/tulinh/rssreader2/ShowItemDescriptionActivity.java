package com.example.tulinh.rssreader2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class ShowItemDescriptionActivity extends AppCompatActivity {

    TextView pubDateView, titleView, descriptionView, seeMore;
    Item currentItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_item_description);

        //Get Intent and Item object
        Intent intent = getIntent();
        currentItem = (Item) intent.getSerializableExtra("Item");

        pubDateView = (TextView) findViewById(R.id.item_pubDate);
        titleView = (TextView) findViewById(R.id.item_title);
        descriptionView = (TextView) findViewById(R.id.item_description);
        seeMore = (TextView) findViewById(R.id.item_seemore);

        // set text for TexViews
        pubDateView.setText(currentItem.getPubDate());
        titleView.setText(currentItem.getTitle());
        descriptionView.setText(currentItem.getDescription());

        seeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create new instance of Intent
                Intent intent = new Intent(ShowItemDescriptionActivity.this,ShowItemWebpageActivity.class);
                intent.putExtra("url string",currentItem.getLink());

                startActivity(intent);
            }
        });


    }
}
